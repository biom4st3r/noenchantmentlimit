package com.biom4st3r.noenchlimit.mixin;

import java.util.Collection;
import java.util.Iterator;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.gen.Accessor;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.*;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.server.command.EnchantCommand;
import net.minecraft.server.command.ServerCommandSource;

@Mixin(EnchantCommand.class)
public class EnchantCommandMixin
{



    @Inject(at = @At("HEAD"),method = "method_13241",cancellable = true) @Accessor()
    private static void method_13241Override(ServerCommandSource serverCommandSource_1, Collection<? extends Entity> collection_1, Enchantment enchantment_1, int int_1, CallbackInfoReturnable<Integer> ci)
    {
        int counter = 0;
        Iterator var5 = collection_1.iterator();
        while(var5.hasNext()) // for the @e @a stuff
        {
            LivingEntity le = (LivingEntity) var5.next();
            ItemStack iS = le.getMainHandStack();
            if(!iS.isEmpty())
            {
                iS.addEnchantment(enchantment_1, int_1);
                counter++;
            }
        }
        ci.setReturnValue(counter);
    } 
}